import React, { Component } from 'react'
import {data} from "../asset/data"
import "./glasses.css"
export default class Glasses extends Component {
    state={
        show:false,
        index:0,
        data:data
    }
    handleChangeGlasses=(index)=>{
        this.setState({
            show:true,
            index:index
        })
    }
  render() {
    return (
      <div className='main'>
        <div className='image'>
            <img src="./glassesImage/model.jpg" alt=''></img>
            {this.state.show&&<img className='image_glasse' src={`./glassesImage/v${this.state.index+1}.png`} alt=''></img>}
            {this.state.show&&<div className='image_info'>
                <h3>{data[this.state.index].name}</h3>
                <p>{data[this.state.index].desc}</p>
            </div>}
            <div>

            </div>
        </div>
        <div className='listGlasses'>
            {data.map((glass,index)=>(
                <div key={index} className="glasses" onClick={()=>this.handleChangeGlasses(index)}>
                    <img src={glass.url} alt=""></img>
                </div>
            ))}
        </div>
      </div>
    )
  }
}
